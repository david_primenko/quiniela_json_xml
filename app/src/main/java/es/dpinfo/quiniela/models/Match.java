package es.dpinfo.quiniela.models;

/**
 * Created by dprimenko on 15/01/17.
 */
public class Match {

    private String team1;
    private String team2;
    private String sig;

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    public Match(String team1, String team2, String sig) {
        this.team1 = team1;
        this.team2 = team2;
        this.sig = sig;
    }

    public Match() {};

    @Override
    public String toString() {
        return "Match{" +
                "team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", sig='" + sig + '\'' +
                '}';
    }
}

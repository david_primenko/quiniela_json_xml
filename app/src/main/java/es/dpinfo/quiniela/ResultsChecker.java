package es.dpinfo.quiniela;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import es.dpinfo.quiniela.models.Match;

/**
 * Created by dprimenko on 16/01/17.
 */
public class ResultsChecker {

    public static void checkSuccessfullyBets(Context context, ArrayList<Match> results, String bet, String method, String filename) {

        String line = "";
        int count = 0;

        for (int i = 0; i < results.size(); i++) {

            if (results.get(i).getSig().equalsIgnoreCase(String.valueOf(bet.charAt(i)))) {
                count++;
            }
        }

        if (count >= 10) {
            if (method.equals("xml")) {
                line += "<ganador>\n\t\t<apuesta>" + bet + "</apuesta>";
                line += "\n\t\t<aciertos>" + count + "</aciertos>\n</ganador>\n";

            } else if (method.equals("json")) {
                line += "{ \"apuesta\" : \"" + bet + "\",";
                line += " \"aciertos\" : \"" + count + "\" },";
            }

            UtilsQuiniela.getInstance().appendPrizeToFile(context, line, filename);
        }
    }
}

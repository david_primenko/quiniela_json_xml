package es.dpinfo.quiniela;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import java.util.ArrayList;

import es.dpinfo.quiniela.interfaces.ICheckerMvp;
import es.dpinfo.quiniela.models.Match;
import es.dpinfo.quiniela.presenters.CheckerPresenter;

/**
 * Created by dprimenko on 15/01/17.
 */
public class MainActivity extends AppCompatActivity implements UtilsQuiniela.ErrorMessageListener, UtilsQuiniela.DataReadyListener, UtilsQuiniela.BetsRequestStatusListener, UtilsQuiniela.UploadPrizesListener, ICheckerMvp.View {

    private CheckerPresenter presenter;
    private LinearLayout llQuiniela;
    private EditText edtResults, edtBets, edtPrizes;
    private Button btnCalculate;
    private RadioButton rdXml, rdJson;
    private ProgressDialog pbDialog;
    private Snackbar snackbar;
    private String method;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new CheckerPresenter(this);
        llQuiniela = (LinearLayout) findViewById(R.id.ll_quiniela);
        edtResults = (EditText) findViewById(R.id.edt_results);
        edtBets = (EditText) findViewById(R.id.edt_bets);
        edtPrizes = (EditText) findViewById(R.id.edt_prizes);
        btnCalculate = (Button) findViewById(R.id.btn_calculate);
        rdJson = (RadioButton) findViewById(R.id.rd_json);
        rdXml = (RadioButton) findViewById(R.id.rd_xml);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rdXml.isChecked()) {
                    UtilsQuiniela.getInstance().requestResults(MainActivity.this, "xml", edtResults.getText().toString());
                } else if (rdJson.isChecked()) {
                    UtilsQuiniela.getInstance().requestResults(MainActivity.this, "json", edtResults.getText().toString());
                }
            }
        });
    }

    @Override
    public void onErrorResponse(String message) {
        Snackbar.make(llQuiniela, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onXMLReady(ArrayList<Match> results) {
        if (presenter.validatePrizeString(edtPrizes.getText().toString(), "xml")) {
            presenter.validateFileBets(this, edtBets.getText().toString());
        }
    }

    @Override
    public void onJsonReady(ArrayList<Match> results) {
        if (presenter.validatePrizeString(edtPrizes.getText().toString(), "json")) {
            presenter.validateFileBets(this, edtBets.getText().toString());
        }
    }

    @Override
    public void showMessageError(String messageError) {
        Snackbar.make(llQuiniela, messageError, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onPreExecuteCheckBets() {
        snackbar = Snackbar.make(llQuiniela, "Buscando ganadores...", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();

        if (rdJson.isChecked()) {
            method = "json";
            UtilsQuiniela.getInstance().appendPrizeToFile(getApplicationContext(), "{ \"ganadores\" : [ ", edtPrizes.getText().toString());
        } else if (rdXml.isChecked()) {
            method = "xml";
            UtilsQuiniela.getInstance().appendPrizeToFile(getApplicationContext(), "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<ganadores>", edtPrizes.getText().toString());
        }
    }

    @Override
    public void onProgressCheckBets(String response) {
        if (response.length() == 16) {
            presenter.checkSuccesfullyBets(getApplicationContext(), response, method, edtPrizes.getText().toString());
        }
    }

    @Override
    public void onPostExecuteCheckBets() {
        if (rdJson.isChecked()) {
            UtilsQuiniela.getInstance().deleteLastCharFromFile(this, edtPrizes.getText().toString());
            UtilsQuiniela.getInstance().appendPrizeToFile(this, "]}", edtPrizes.getText().toString());
        } else if (rdXml.isChecked()) {
            UtilsQuiniela.getInstance().appendPrizeToFile(this, "</ganadores>", edtPrizes.getText().toString());
        }

        snackbar.dismiss();
        presenter.uploadFile(this, UtilsQuiniela.URL_UPLOAD, edtPrizes.getText().toString());
    }

    @Override
    public void onStartUpload() {
        snackbar = Snackbar.make(llQuiniela, "Subiendo los premios...", Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    public void onSuccessUpload(String response) {
        snackbar = Snackbar.make(llQuiniela, response, Snackbar.LENGTH_LONG);
        snackbar.show();
        presenter.deleteTempFile(this, edtPrizes.getText().toString());
    }

    @Override
    public void onErrorUpload(String error) {
        snackbar = Snackbar.make(llQuiniela, error, Snackbar.LENGTH_LONG);
        snackbar.show();
        presenter.deleteTempFile(this, edtPrizes.getText().toString());
    }
}

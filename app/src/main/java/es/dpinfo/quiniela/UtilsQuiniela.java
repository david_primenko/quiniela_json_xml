package es.dpinfo.quiniela;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.HttpGet;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import es.dpinfo.quiniela.models.Match;

/**
 * Created by dprimenko on 15/01/17.
 */
public class UtilsQuiniela {

    public static final String URL_UPLOAD = "http://alumno.club/superior/david/upload.php";
    private RequestQueue queue;
    private ErrorMessageListener errorMessageListener;
    private DataReadyListener dataReadyListener;
    private JsonRequestStatusListener jsonRequestStatusListener;
    private XmlRequestStatusListener xmlRequestStatusListener;
    private BetsRequestStatusListener betsRequestStatusListener;
    private UploadPrizesListener uploadPrizesListener;
    private AppendToFileListener appendToFileListener;
    private static final String URL_RESULTS_XML = "https://dpinfo.es/quiniela/resultados.xml";
    private static final String URL_RESULTS_JSON = "https://dpinfo.es/quiniela/resultados.json";
    private JSONObject jsonObject;
    private ArrayList<Match> results;
    private String response;

    public interface BetsRequestStatusListener {
        void onPreExecuteCheckBets();
        void onProgressCheckBets(String response);
        void onPostExecuteCheckBets();
    }

    public interface XmlRequestStatusListener {
        void onResponseSuccess(String xml);
        void onErrorResponse(CharSequence message);
    }

    public interface JsonRequestStatusListener {
        void onResponseSuccess(JSONObject response);
        void onErrorResponse(CharSequence message);
    }

    public interface AppendToFileListener {
        void onSuccessCreate();
        void onFailureCreate();
    }

    public interface UploadPrizesListener {
        void onStartUpload();
        void onSuccessUpload(String responseString);
        void onErrorUpload(String error);
    }

    public interface ErrorMessageListener {
        void onErrorResponse(String message);
    }

    public interface DataReadyListener {
        void onXMLReady(ArrayList<Match> results);
        void onJsonReady(ArrayList<Match> results);
    }

    private static UtilsQuiniela ourInstance = new UtilsQuiniela();

    public static UtilsQuiniela getInstance() {
        return ourInstance;
    }

    private UtilsQuiniela() {
    }

    private void getResultsJson(Context context, String url) {
        queue = Volley.newRequestQueue(context);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        jsonRequestStatusListener.onResponseSuccess(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        jsonRequestStatusListener.onErrorResponse(error.toString());
                    }
                });
        queue.add(getRequest);
    }

    private void getResultsXml(Context context, String url) {

        queue = Volley.newRequestQueue(context);

        StringRequest getRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                xmlRequestStatusListener.onResponseSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                xmlRequestStatusListener.onErrorResponse(error.toString());
            }
        });


        queue.add(getRequest);
    }

    public void requestBets(Context context, String url) {
        try {
            betsRequestStatusListener = (BetsRequestStatusListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement BetsRequestStatusListener");
        }

        new GetBets().execute(url);
    }

    public void requestResults(Context context, final String method, String url) {

        try {
            errorMessageListener = (ErrorMessageListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement ErrorMessageListener");
        }

        try {
            dataReadyListener = (DataReadyListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement DataReadyListener");
        }

        if (method.equals("json")) {

            jsonRequestStatusListener = new JsonRequestStatusListener() {
                @Override
                public void onResponseSuccess(JSONObject response) {

                    JSONArray jsonArray = null;

                    try {
                         jsonArray = response.getJSONObject("quiniela").getJSONArray("match");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    results = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            results.add(new Match(
                                    jsonArray.getJSONObject(i).getString("team1"),
                                    jsonArray.getJSONObject(i).getString("team2"),
                                    jsonArray.getJSONObject(i).getString("sig")
                            ));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    dataReadyListener.onJsonReady(results);
                }

                @Override
                public void onErrorResponse(CharSequence message) {
                    errorMessageListener.onErrorResponse(message.toString());
                }
            };

            getResultsJson(context, url);

        } else if (method.equals("xml")) {
            xmlRequestStatusListener = new XmlRequestStatusListener() {
                @Override
                public void onResponseSuccess(String xml) {
                    boolean error = false;
                    XmlPullParser xpp = null;
                    XmlPullParserFactory factory = null;

                    try {
                        factory = XmlPullParserFactory.newInstance();
                        factory.setNamespaceAware(true);
                        xpp = factory.newPullParser();
                        xpp.setInput(new StringReader(xml));
                    } catch (XmlPullParserException e) {
                        results = null;
                        error = true;
                        e.printStackTrace();
                    }

                    int eventType = 0;

                    try {
                        eventType = xpp.getEventType();
                    } catch (XmlPullParserException e) {
                        results = null;
                        error = true;
                        e.printStackTrace();
                    }

                    Match match = null;
                    boolean inMatch = false;


                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        try {
                            String tagName = null;

                            switch (eventType) {
                                case XmlPullParser.START_DOCUMENT:
                                    results = new ArrayList<>();
                                    break;
                                case XmlPullParser.START_TAG:

                                    tagName = xpp.getName();

                                    if (tagName.equals("match")) {
                                        inMatch = true;
                                        match = new Match();
                                    }
                                    else if (inMatch && tagName.equals("team1")) {
                                        match.setTeam1(xpp.nextText());
                                    } else if (inMatch && tagName.equals("team2")) {
                                        match.setTeam2(xpp.nextText());
                                    }
                                    else if (inMatch && tagName.equals("sig")) {
                                        match.setSig(xpp.nextText());
                                    }
                                    break;
                                case XmlPullParser.END_TAG:
                                    if (xpp.getName().equals("match")) {
                                        inMatch = false;
                                        try {
                                            results.add(match);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    break;
                            }

                            eventType = xpp.next();
                        } catch (XmlPullParserException e) {
                            results = null;
                            error = true;
                            e.printStackTrace();
                        } catch (IOException e) {
                            results = null;
                            error = true;
                            e.printStackTrace();
                        }
                    }

                    if (!error) {
                        dataReadyListener.onXMLReady(results);
                    } else {
                        errorMessageListener.onErrorResponse("El archivo no tiene un formato correcto");
                    }
                }

                @Override
                public void onErrorResponse(CharSequence message) {
                    errorMessageListener.onErrorResponse(message.toString());
                }
            };

            getResultsXml(context, url);
        }
    }

    public ArrayList<Match> getResults () {
        return results;
    }

    public void deleteLastCharFromFile(Context context, String filename) {
        File file = new File(context.getFilesDir().getPath(), filename);
        try {
            FileChannel fileChannel = new FileOutputStream(file, true).getChannel();
            fileChannel.truncate(fileChannel.size() - 1);
            fileChannel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendPrizeToFile(Context context, String content, String filename) {

        File file = null;
        BufferedWriter writer = null;

        file = new File(context.getFilesDir().getPath(), filename);

        if (file.exists()) {
            try {
                writer = new BufferedWriter(new FileWriter(file, true));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            try {
                file.createNewFile();
                writer = new BufferedWriter(new FileWriter(file, false));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            writer.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteFile(Context context, String filename) {
        try{
            File file = new File(context.getFilesDir().getPath(), filename);
            file.delete();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void readFile(Context context, String filename) {
        try{
            File file = new File(context.getFilesDir().getPath(), filename);
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String linea = "";
            String result = "";

            while ((linea = reader.readLine()) != null) {
                result = linea;
            }

            Log.d("Linea", result);

            reader.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void uploadPrizes(Context context, String url, String file) {
        try {
            uploadPrizesListener = (UploadPrizesListener) context;
        } catch (ClassCastException e ) {
            throw new ClassCastException(context.toString() + " must implement UploadPrizesListener");
        }

        final File uploadFile = new File(context.getFilesDir().getPath(), file);
        RequestParams params = new RequestParams();

        try {
            params.put("fileToUpload", uploadFile);
        } catch (FileNotFoundException e) {
            errorMessageListener.onErrorResponse("El archivo de premios no se pudo crear");
        }

        RestClient.post(url, params, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                uploadPrizesListener.onStartUpload();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                uploadPrizesListener.onErrorUpload(responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                uploadPrizesListener.onSuccessUpload(responseString);
            }
        });
    }

    private class GetBets extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            betsRequestStatusListener.onPreExecuteCheckBets();
        }

        @Override
        protected Boolean doInBackground(String... urls) {
            boolean error = false;
            response = "";

            for (String url : urls) {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                try {
                    HttpResponse execute = client.execute(httpGet);
                    InputStream content = execute.getEntity().getContent();

                    if (!content.equals("")) {
                        BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
                        String s = "";
                        while ((s = buffer.readLine()) != null) {
                            response = s;
                            betsRequestStatusListener.onProgressCheckBets(response);
                        }
                        response = null;
                    } else {
                        error = true;
                    }

                } catch (Exception e) {
                    error = true;
                    e.printStackTrace();
                }
            }

            return error;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            betsRequestStatusListener.onPostExecuteCheckBets();
        }
    }
}

package es.dpinfo.quiniela.interfaces;

import android.content.Context;

import java.util.ArrayList;

import es.dpinfo.quiniela.models.Match;

/**
 * Created by dprimenko on 16/01/17.
 */
public interface ICheckerMvp {

    interface View {
        void showMessageError(String messageError);
    }

    interface Presenter {
        boolean validatePrizeString(String prizeStrings, String method);
        void validateFileBets(Context context, String urlBets);
        void checkSuccesfullyBets(Context context, String response, String method, String filename);
        void uploadFile(Context context, String url, String file);
        void deleteTempFile(Context context, String filename);
    }
}

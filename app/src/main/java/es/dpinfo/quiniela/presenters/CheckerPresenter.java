package es.dpinfo.quiniela.presenters;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import es.dpinfo.quiniela.ResultsChecker;
import es.dpinfo.quiniela.UtilsQuiniela;
import es.dpinfo.quiniela.interfaces.ICheckerMvp;
import es.dpinfo.quiniela.models.Match;

/**
 * Created by dprimenko on 16/01/17.
 */
public class CheckerPresenter implements ICheckerMvp.Presenter {

    private ICheckerMvp.View view;

    public CheckerPresenter(ICheckerMvp.View view) {
        this.view = view;
    }

    @Override
    public boolean validatePrizeString(String prizeStrings, String method) {

        boolean result = false;

        if (method.equals("xml")) {
            if (prizeStrings.endsWith(".xml")) {
                result = true;
            } else {
                view.showMessageError("El archivo de premios debe tener extensión XML");
            }
        } else if (method.equals("json")) {
            if (prizeStrings.endsWith(".json")) {
                result = true;
            } else {
                view.showMessageError("El archivo de premios debe tener extensión JSON");
            }
        }

        return result;
    }

    @Override
    public void validateFileBets(Context context, String urlBets) {

        if (urlBets.endsWith(".txt")) {
            UtilsQuiniela.getInstance().requestBets(context, urlBets);
        } else {
            view.showMessageError("Las apuestas no tienen un formato valido");
        }
    }

    @Override
    public void checkSuccesfullyBets(Context context, String response, String method, String filename) {
        ResultsChecker.checkSuccessfullyBets(context, UtilsQuiniela.getInstance().getResults(), response, method, filename);
    }

    @Override
    public void uploadFile(Context context, String url, String file) {
        UtilsQuiniela.getInstance().uploadPrizes(context, url, file);
    }

    @Override
    public void deleteTempFile(Context context, String filename) {
        UtilsQuiniela.getInstance().deleteFile(context, filename);
    }
}
